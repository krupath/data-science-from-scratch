# data-science-from-scratch

This repository logs my process of working through the 'Data Science from Scratch' book by Joel Grus.  

Every milestone in the project corresponds to a chapter in the book, as listed below: 

1. Introduction
2. A Crash Course in Python
3. Visualizing Data
4. Linear Algebra
5. Statistics
6. Probability
7. Hypothesis and Inference
8. Gradient Descent
9. Getting Data
10. Working With Data
11. Machine Learning
12. k-Nearest Neighbors
13. Naive Bayes
14. Simple Linear Regression
15. Multiple Regression
16. Logistic Regression
17. Decision Trees
18. Neural Networks
19. [Deep Learning]
20. Clustering
21. Natural Language Processing
22. Network Analysis
23. Recommender Systems
24. Databases and SQL
25. MapReduce
26. Data Ethics
27. Go Forth And Do Data Science

Source: https://github.com/joelgrus/data-science-from-scratch

